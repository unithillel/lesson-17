<?php
spl_autoload_register( function ($className) {
    $className = str_replace('\\', DIRECTORY_SEPARATOR, $className).'.php';
    require_once $className;
});
require_once 'application/bootstrap.php';
