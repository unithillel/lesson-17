<?php


namespace application\controllers;

use application\core\Controller;

class HomeController extends Controller
{

    public function actionIndex(){
        $this->view->generate('home/index.php');
    }

    public function actionContacts(){
        $this->view->generate('home/contacts.php');
    }
}