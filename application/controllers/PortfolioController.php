<?php


namespace application\controllers;


use application\core\Controller;
use application\models\PortfolioModel;

class PortfolioController extends Controller
{
    
    
    public function actionIndex(){
        $data['portfolio'] = PortfolioModel::all();
        $this->view->generate('portfolio/index.php', $data);
    }

}