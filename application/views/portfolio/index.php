<h1>Portfolio</h1>
<table>
    <tr>
        <th>Title</th>
        <th>Year</th>
        <th>Url</th>
    </tr>
<?php foreach ($data['portfolio'] as $work):?>
    <tr>
        <td><?=$work['title']?></td>
        <td><?=$work['year']?></td>
        <td><a href="<?=$work['url']?>">Visit website > </a></td>
    </tr>
<?php endforeach;?>
</table>
